import numpy


def sum_mod_2(a, b):
    # сложение по модулю 2
    return (a + b) % 2


def apply_polynomial(polynomial, registers):
    # polynomial - массив numpy, содержащий коэффициенты полинома
    # registers - массив numpy, содержащий биты сдвиговых регистров
    return numpy.sum(polynomial * registers) % 2


def equals(arr1, arr2):
    # Возвращает True, если все значения из arr1 совпадают со значениями из arr2
    return numpy.all(numpy.equal(arr1, arr2))


def distance(arr1, arr2):
    # Расстояние между битовыми массивами - количество позиций, в которых они отличаются. Если сложить два массива по
    # модулю 2, то получится массив, который содержит единицы только на тех местах, где эти два массива отличаются. Если
    # после этого просуммировать полученный массив, получим количество отличающихся элементов
    return numpy.sum((arr1 + arr2) % 2)


def account_ambiguities(arr1, arr2):
    # Функция сравнивает два массива, возвращает массив, у которого элементы, которые у arr1 и arr2 не совпадают,
    # заменены на None
    result = []
    for i in range(len(arr1)):
        if arr1[i] == arr2[i]:
            result.append(arr1[i])
        else:
            result.append(None)
    return result
