import numpy

from scripts.encryption import ConvCode

if __name__ == '__main__':
    # Строим свёрточный код с порождающими многочленами: g1 = 1101
    #                                                    g2 = 1011
    code = ConvCode([
        numpy.array([1, 1, 0, 1], dtype=int),
        numpy.array([1, 0, 1, 1], dtype=int)
    ])

    # Составляем сообщение: 10100
    input_message = numpy.array([1, 0, 1, 0, 0])
    # Кодируем составленное сообщение, должны получить: 1110100101
    output_message = code.encode(input_message)
    print()
    print('Пример 1. Кодирование сообщения. 10100 -> 1110100101')
    print(f'Входное сообщение: {input_message}')
    print(f'Выходное сообщение: {output_message}')
    print()
    print('Пример 1. Декодирование сообщения. 1110100101 -> 10100')
    print(f'Входное сообщение: {output_message}')
    print(f'Выходное сообщение: {code.decode(output_message)}')
    print()

    # Составляем закодированное сообщение: 001101110101011100
    input_message = numpy.array([0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0])
    # Декодируем составленное сообщение, должны получить: 011010000
    output_message = code.decode(input_message)
    print('Пример 2. Декодирование сообщения. 001101110101011100 -> 011010000')
    print(f'Входное сообщение: {input_message}')
    print(f'Выходное сообщение: {output_message}')
    print('Пример 2. Кодирование сообщения. 011010000 -> 001101110101011100')
    print(f'Входное сообщение: {output_message}')
    print(f'Выходное сообщение: {code.encode(output_message)}')
    print()

    # Составляем сообщение: 11 00 00 00 00 00 00 00
    input_message = numpy.array([1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    # Декодируем составленное сообщение, должны получить: 00 00 00 00 00 00 00 00
    output_message = code.decode_viterbi(input_message, 7)
    print('Пример 3. Алгоритм Витерби. 11 00 00 00 00 00 00 00 -> 00 00 00 00 00 00 00 00')
    print(f'Входное сообщение: {input_message}')
    print(f'Выходное сообщение: {output_message}')
    print()

    # Составляем сообщение: 11 00 00 00 10 00 00 00
    input_message = numpy.array([1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0])
    # Декодируем составленное сообщение, должны получить: 1...
    output_message = code.decode_viterbi(input_message, 7)
    print('Пример 4. Алгоритм Витерби. 11 00 00 00 10 00 00 00 -> 1...')
    print(f'Входное сообщение: {input_message}')
    print(f'Выходное сообщение: {output_message}')
    print()
